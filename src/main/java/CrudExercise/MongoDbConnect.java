package CrudExercise;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MongoDbConnect implements IConnect {


    protected MongoClient mongoClient;

    @Override
    public void MakeConnection() {

        mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        String connectPoint = mongoClient.getConnectPoint();
        System.out.println(connectPoint);
        System.out.println("Connected to the database successfully");

    }

    @Override
    public void CloseConnection() {
        mongoClient.close();
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public void setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }
}
