package CrudExercise;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

//        MongoDbConnect mongoDbConnect = new MongoDbConnect();
//        MongoCRUD mongoCRUD = new MongoCRUD();
//
//        mongoDbConnect.MakeConnection();
//
//        mongoCRUD.DatabaseInUse();
//        mongoCRUD.Create();
//        mongoCRUD.InsertValues();
//        mongoCRUD.Read();

        JdbcCRUD jdbcCRUD = new JdbcCRUD();

        jdbcCRUD.MakeConnection();
        jdbcCRUD.Create();
        jdbcCRUD.InsertValues();
        jdbcCRUD.Read();

    }
}
