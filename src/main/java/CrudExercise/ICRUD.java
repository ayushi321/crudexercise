package CrudExercise;

public interface ICRUD {

    public void DatabaseInUse();

    public void Create();

    public void InsertValues();

    public void Read();

    public void Update();

    public void Delete();
}
