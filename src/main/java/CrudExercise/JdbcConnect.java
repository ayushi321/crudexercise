package CrudExercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class JdbcConnect implements IConnect {

    private String url;
    private String username;
    private String password;

    protected Connection currentconnection;

    public JdbcConnect() {
        this.url = null;
        this.username = null;
        this.password = null;
    }

    @Override
    public void MakeConnection() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter url for mysql connection : ");
        url = sc.next();

        System.out.println("Enter username : ");
        username = sc.next();

        System.out.println("Enter password : ");
        password = sc.next();

        try{
            currentconnection = DriverManager.getConnection(url,username,password);
            System.out.println("Successfully Connected");
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public void CloseConnection()  {
        try{
        currentconnection.close();
            System.out.println("Connection closed");
        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    public Connection getCurrentconnection() {
        return currentconnection;
    }

    public void setCurrentconnection(Connection currentconnection) {
        this.currentconnection = currentconnection;
    }
}
