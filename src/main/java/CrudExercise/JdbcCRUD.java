package CrudExercise;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcCRUD extends JdbcConnect implements ICRUD {


    private String createTableQuery,email,name;
    private Statement statement;
    private int id,age;


    @Override
    public void DatabaseInUse() {

    }

    @Override
    public void Create() {

        createTableQuery = "CREATE TABLE  User1"+ "(" +

                "    id INT(11), " +

                "    name VARCHAR(255)," +

                "    age INT(11)," +

                "    email VARCHAR(255)" +

                ")";
        try{
           statement =  currentconnection.createStatement();
           statement.executeUpdate(createTableQuery);

           System.out.println("Table created successfully");
       }catch (SQLException e){
           System.out.println("Table already exists");
           e.printStackTrace();
       }
    }

    @Override
    public void InsertValues() {
      try {
          statement = currentconnection.createStatement();
          String insertValues = "INSERT INTO User1"
                                +"(id, name, age, email)" +"VALUES"
                                + "(1, 'LUNA', 23, 'luna@gmail.com')";
          statement.executeUpdate(insertValues);

          System.out.println("Values inserted");
      }catch (SQLException e ){
          e.printStackTrace();
      }
    }

    @Override
    public void Read() {
     try {
         System.out.println("Reading table data");
         statement = currentconnection.createStatement();
         ResultSet resultSet = statement.executeQuery("select * from User1");

         while (resultSet.next()){
             id = resultSet.getInt(1);
             name = resultSet.getString(2);
             age = resultSet.getInt(3);
             email = resultSet.getString(4);

             System.out.println("Id = " + String.valueOf(id) + " name = " + name + " age = " + String.valueOf(age) +

                     " email = " + email);
         }
     }catch (SQLException | NullPointerException e){
         e.printStackTrace();
     }
    }

    @Override
    public void Update() {

    }

    @Override
    public void Delete() {

    }
}
