package CrudExercise;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.Iterator;

public class MongoCRUD extends MongoDbConnect implements ICRUD {

    private MongoDatabase db;
    private MongoCollection<Document> collectionName;

    @Override
    public void DatabaseInUse() {

        db = mongoClient.getDatabase("admin");
        System.out.println("database admin is in use");
    }

    @Override
    public void Create() {
      collectionName = db.getCollection("Patient");
    }

    @Override
    public void InsertValues() {
        Document document = new Document("patient_name", "Chelsey")
                               .append("patient_adress", "12-B Suryavillas,Gurgaon")
                               .append("gender", "Female")
                               .append("age", "34")
                               .append("patient_code", "125890")
                               .append("allergies", "Wheezing");
               collectionName.insertOne(document);
               System.out.println("Document inserted successfully");
    }

    @Override
    public void Read() {
        FindIterable<Document> iterDoc = collectionName.find();
        int i = 1;
        Iterator it = iterDoc.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
            i++;
        }
        System.out.println("Reading Documents");

    }

    @Override
    public void Update() {

    }

    @Override
    public void Delete() {

    }
}
