package CrudExercise;

public interface IConnect {

     void MakeConnection();

     void CloseConnection();
}
